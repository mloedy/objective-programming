package Zadanie20;

public class Recatangle extends Figure {
    protected double a;
    protected double b;

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public Recatangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double countArea() {
        double rectangleArea=a*b;
        return rectangleArea;
    }

    @Override
    public double countPerimeter() {
        double rectanglePerimiter=(2*a)+(2*b);
        return rectanglePerimiter;
    }
}
