package Zadanie20;

public class Circle extends Figure {
    protected double r;

    public Circle(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double countArea() {
        return Math.PI*(r*r);
    }

    @Override
    public double countPerimeter() {
        return 2*r*Math.PI;
    }
}
