package Zadanie20;

public class Square extends Figure{
    protected double a;

    public Square(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    @Override
    public double countArea() {
        double squareArea=a*a;
        return squareArea;
    }

    @Override
    public double countPerimeter() {
        double squarePerimeter=4*a;
        return squarePerimeter;
    }
}
