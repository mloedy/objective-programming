package Zadanie10;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Car car = new Car();
        String line;
        String mark = "Ford Mustang";
        int cc = 5000;
        int KM = 312;
        int distance = 57000;
        int year = 1968;



        System.out.println("what do You want to do? (Mark/AddPassenger/RemovePassenger/CheckSlots/" +
                "IncreaseSpeed/DecreaseSpeed/CheckSpeed/CC/KM/Distance/Year)");
        do {
            System.out.println("Give me Command: ");
            line = scanner.nextLine();
            if(line.equals("Mark")){
                System.out.println("Mark of the car is: "+car.mark(mark));
            }
            if(line.equals("CC")){
                System.out.println("Car's engine capacity is: "+car.cc(cc));
            }
            if(line.equals("KM")){
                System.out.println("Car's HP is: "+car.km(KM));
            }
            if (line.equals("Distance")){
                System.out.println("Car made: "+car.distanceDone(distance)+" km");
            }
            if (line.equals("Year")){
                System.out.println("Car has "+car.year(year)+" years old");
            }
            if (line.equals("IncreaseSpeed")){
                double speedadd = scanner.nextDouble();
                System.out.println("You have increased Your speed to: "+car.speedIncrease(speedadd)+"km/h"+scanner.nextLine());
            }
            if (line.equals("DecreaseSpeed")){
                double speedsubstract = scanner.nextDouble();
                System.out.println("You have decreased Your speed to: "+car.speedDecrease(speedsubstract)+"km/h"+scanner.nextLine());
            }
            if (line.equals("CheckSpeed")){
                System.out.println("Your speed is: "+car.speedCheck());
            }
            if (line.equals("AddPassenger")){
                System.out.println("You have taken Passenger. Now there are "+car.AddPassenger(1)+" passengers");
            }
            if (line.equals("RemovePassenger")){
                System.out.println("You have removed Passenger. Now there are "+car.RemovePassenger(1)+" pasengers");
            }



        } while (!line.equals("quit"));
    }
}
