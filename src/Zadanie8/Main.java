package Zadanie8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line;
        BankAccount bankAccount = new BankAccount();
        do {
            System.out.println("What do You want to do? (add/substract/check)");
            line = scanner.nextLine();
            if (line.equals("add")) {
                System.out.println("How much do you want to add?");
                double adding = scanner.nextDouble();
                System.out.println("Added: " + adding + " your account status is: " + bankAccount.addMoney(adding) + scanner.nextLine());
            } else if (line.equals("substract")) {
                System.out.println("How much do you want to substract?");
                double substracting = scanner.nextDouble();
                System.out.println("Substracted: " + substracting + " your account status is: " + bankAccount.substractMoney(substracting) + scanner.nextLine());
            } else if (line.equals("check")) {
                System.out.println("Your bank status is: " + bankAccount.accountStatus);
            } else {
                System.out.println("Wrong command.");
            }

        } while (!line.equals("quit"));
    }
}