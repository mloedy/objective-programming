package Zadanie7;

public class Kolo {
    public double poleKola(double r){
        double result = 3.14 * (r*r);
        return result;
    }
    public double obwodKola(double r){
        double result = (2*r)*3.14;
        return result;
    }
}
