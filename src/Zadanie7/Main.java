package Zadanie7;

public class Main {
    public static void main(String[] args) {
        Kolo kolo = new Kolo();
        Kwadrat kwadrat = new Kwadrat();
        Prostokat prostokat = new Prostokat();
        double aKwadrat=15;
        double aProstokat=7;
        double bProstokat=12;
        double rKolo=9;
        double poleKwadratu = kwadrat.polekwadratu(aKwadrat);
        System.out.println("pole kwadratu "+poleKwadratu);
        double obwodKwadratu = kwadrat.obwodkwadratu(aKwadrat);
      System.out.println("obwod kwadratu " + obwodKwadratu);
      double poleProstokata = prostokat.poleprostokata(aProstokat,bProstokat);
      System.out.println("pole prostokata "+poleProstokata);
      double obwodProstokata = prostokat.obwodprostokata(aProstokat,bProstokat);
      System.out.println("obwod prostokata "+obwodProstokata);
      double poleKola = kolo.poleKola(rKolo);
      System.out.println("pole kola "+poleKola);
      double obwodKola = kolo.obwodKola(rKolo);
      System.out.println("obwod kola "+obwodKola);

    }
}
