package Zadanie35;


public class Client {
    private String pesel;
    private ClientCase sprawa;
    public Client(String pesel, ClientCase sprawa) {
        super();
        this.pesel = pesel;
        this.sprawa = sprawa;
    }
    public String getPesel() {
        return pesel;
    }
    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
    public ClientCase getSprawa() {
        return sprawa;
    }
    public void setSprawa(ClientCase sprawa) {
        this.sprawa = sprawa;
    }
    @Override
    public String toString() {
        return "Client [pesel=" + pesel + ", sprawa=" + sprawa + "]";
    }


}