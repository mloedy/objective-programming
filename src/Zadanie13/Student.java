package Zadanie13;

import java.util.ArrayList;

public class Student {
    private int indexNumber;
    private String name;
    private String surname;
    private ArrayList<Double> gradeList;

    ////srednia ocen


    private ArrayList<Double> averangeGrade;

    public double averange(ArrayList<Double> gradeList) {
        double suma = 0;
        for (int i = 0; i < gradeList.size(); i++) {
            suma += gradeList.get(i);
        }
        double averangeGrade = suma / gradeList.size();
        return averangeGrade;
    }



    ////// false przy ocenie 1 albo 2


    private ArrayList<Double> TFgrade;

    public boolean TFgrade(ArrayList<Double> gradeList) {
        boolean f=true;
        for (int i = 0; i < gradeList.size(); i++) {
            if(gradeList.get(i)<=2){
            f=false;
            }
        }
        return f;
    }

    public Student(int indexNumber, String name, String surname, ArrayList<Double> gradeList, ArrayList<Double> averangeGrade) {
        this.indexNumber = indexNumber;
        this.name = name;
        this.surname = surname;
        this.gradeList = gradeList;
        this.averangeGrade = averangeGrade;
    }

    public int getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(int indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public ArrayList<Double> getGradeList() {
        return gradeList;
    }

    public void setGradeList(ArrayList<Double> gradeList) {
        this.gradeList = gradeList;
    }

    public ArrayList<Double> getAverangeGrade(ArrayList<Double> gradeList) {
        return averangeGrade;
    }

    public void setAverangeGrade(ArrayList<Double> averangeGrade) {
        this.averangeGrade = averangeGrade;
    }

    @Override
    public String toString() {
        return "Student{" +
                "indexNumber=" + indexNumber +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", gradeList=" + gradeList;
    }

}





