package Zadanie14;

public class Package {
    private String receiver;
    private String sender;
    boolean isPackageSent=false;
    boolean isRegisteredPackageSent=false;
    private boolean isContent;

    ////         - ma metodę 'wyślij' która sprawdza czy jest zawartość, i czy jest odbiorca i wyświetla komunikat
    // o nadaniu jeśli jest zawartość i odbiorca,
    //oraz ustawia flagę 'czy została wysłana' na true.

    public boolean send(String receiver, boolean isContent){
        if ((receiver!=null)&&(isContent==true)){
            isPackageSent=true;
        }return isPackageSent;
    }
    public boolean sendRegistered(String receiver,String sender, boolean isContent){
        if ((receiver!=null)&&(isContent==true)&&(sender!=null)){
            isRegisteredPackageSent=true;
        }return isRegisteredPackageSent;
    }


    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public boolean isPackageSent() {
        return isPackageSent;
    }

    public void setPackageSent(boolean packageSent) {
        isPackageSent = packageSent;
    }

    public boolean isContent() {
        return isContent;
    }

    public void setContent(boolean content) {
        isContent = content;
    }

    public Package() {
    }

    public Package(String receiver, String sender, boolean isContent) {
        this.receiver = receiver;
        this.sender = sender;
        this.isContent = isContent;
    }

    @Override
    public String toString() {
        return "Package{" +
                "receiver='" + receiver + '\'' +
                ", sender='" + sender + '\'' +
                ", isPackageSent=" + isPackageSent +
                ", isContent=" + isContent +
                '}';
    }
}
