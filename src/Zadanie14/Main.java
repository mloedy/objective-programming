package Zadanie14;

public class Main {
    public static void main(String[] args) {
        String receiver1 = "Jan";
        String sender1 = "Michał";
        boolean isContent1 = true;
    Package pack1 = new Package(receiver1, sender1,isContent1);
        System.out.println("is registered package sent?: "+ pack1.sendRegistered(receiver1,sender1,isContent1));
        System.out.println("is package sent?: "+ pack1.send(receiver1,isContent1));
        String receiver2 = "Jan";
        String sender2 = null;
        boolean isContent2 = true;
    Package pack2 = new Package(receiver2, sender2,isContent2);
        System.out.println("is registered package sent?: "+ pack2.sendRegistered(receiver2,sender2,isContent2));
        System.out.println("is package sent?: "+ pack2.send(receiver2,isContent2));

    }
}
