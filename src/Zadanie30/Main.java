package Zadanie30;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Formatter;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        String date;
        boolean error = false;
        do {

            Scanner scanner = new Scanner(System.in);
            System.out.println("podaj (ROK-MSC-DZIEN GODZINA:DATA) ");
            date = scanner.nextLine();
            if (date.equals("quit")) {
                System.out.println("bye! ");
                break;
            }
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

                LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
                System.out.println(localDateTime);
            } catch (DateTimeParseException n) {
                error = true;
                System.out.println("Wrong date format ");
            }
            if (error == false) {
                System.out.println("Properly date format");
            }
        } while (!date.equals("quit"));
    }
}
