package Zadanie15;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Phone phone = new Phone("phone is calling to: ");
        String line;
        do {
            System.out.println("commands: call/cancel/quit");
            line = scanner.nextLine();
            String[] commands = line.split(" ");
            if (commands[0].equals("call")) {
                System.out.println(phone.screen + phone.numberChoosen(commands[1]));
            }
            if (commands[0].equals("cancel")) {
                System.out.println("phone is calling: " + !phone.call(line));
            }

        } while (!line.equals("quit"));
    }
}

