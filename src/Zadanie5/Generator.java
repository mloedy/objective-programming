package Zadanie5;

import java.util.Random;
//a
public class Generator {
//    public int generateNext(int liczba1) {
//        int generated=liczba1++;
//        return generated;
//    }
////b
//    public int generaterandom(int liczba1, int random) {
//        Random random1 = new Random(random);
//        int losowa = random1.nextInt();
//        int generated = liczba1+losowa;
//        return generated;
//    }}
private int number = 0;

    private int minRange;
    private int maxRange;
    private int numberInRange;

    public Generator(int minRange, int maxRange) {
        this.minRange = minRange;
        this.maxRange = maxRange;
    }

    // a
    public int generateNumber() {
//        int result = number;
//        number++;
//
//        return result;
//        return number++;
        return number++;
    }

    // b
    public int generateNumberInRange() {
        Random randomGenerator = new Random();
        int rangeSize = maxRange - minRange;

        return randomGenerator.nextInt(rangeSize) + minRange;
    }

    public int generateNumberInRange2() {
        int result = minRange + numberInRange;
        numberInRange++;

        int rangeSize = maxRange - minRange;
        numberInRange = numberInRange % rangeSize;

        return result;
    }

    // c
    public int generateNextEven() {
        return (number++) * 2;
    }

    // c*
    public int generateNextOdd() {
        return (number++) * 2 + 1;
    }

    // d
    public int generateNextMult(int multiplied) {
        return (number++) * multiplied;
    }

    public boolean generateTrueFalse() {
        return ((number++) % 2 == 0);
    }

    public char generateChar() {
        int whichLetter = (number++) % 26;
        int asciiCodeOfLetter = whichLetter + 97;

        return (char) asciiCodeOfLetter;
    }

}

