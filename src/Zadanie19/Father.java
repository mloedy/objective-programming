package Zadanie19;

public class Father extends FamilyMember {
    public Father(String name, String surname, int age){
        super(age, name, surname);
    }
    @Override
    public void introduce(){
        System.out.println("i am father, my name is: "+name+surname);}
    @Override
    public boolean isAdult() {
        return true;
    }
}
