package Zadanie19;

public class Daughter extends FamilyMember {
    public Daughter(String name, String surname, int age){
        super(age, name, surname);
    }
    @Override
    public void introduce(){
        System.out.println("I am daughter, my name is: "+name+surname);}
    @Override
    public boolean isAdult() {
        boolean adult=age>=18;
        return adult;
    }
}
