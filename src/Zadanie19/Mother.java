package Zadanie19;

public class Mother extends FamilyMember{
    public Mother(String name, String surname, int age){
        super(age, name, surname);
    }
    @Override
    public void introduce(){
        System.out.println("i am mother, my name is: "+name+surname);
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
