package Zadanie19;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
    FamilyMember mother = new Mother("Anka ","Tak ",40);
    FamilyMember father = new Father("Jan ","tak ",42);
    FamilyMember daughter = new Daughter("Zosia ", "Tak",15);
    FamilyMember son = new Son("Ziutek ", "tak ",13);
        ArrayList<FamilyMember>family=new ArrayList<>();
        family.add(mother);
        family.add(father);
        family.add(daughter);
        family.add(son);
        for (FamilyMember member : family) {
            member.introduce();

        }
        System.out.println("Is Adult?");
        for(FamilyMember member:family) {
            System.out.println(member.isAdult());
        }
    }
}
