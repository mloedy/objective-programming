package Zadanie19;

public abstract class FamilyMember {
    protected int age;
    protected String name;
    protected String surname;


    public FamilyMember(int age, String name, String surname) {
        this.age = age;
        this.name = name;
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void introduce(){
        System.out.println("I am just a simple family member");
    }
    public abstract boolean isAdult();
}
