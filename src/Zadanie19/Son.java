package Zadanie19;

public class Son extends FamilyMember {
    public Son(String name, String surname, int age){
        super(age, name, surname);
    }
    @Override
    public void introduce(){
        System.out.println("i am son, my name is: "+name+surname);}
    @Override
    public boolean isAdult() {
        boolean adult=age>=18;
        return adult;
    }
}
