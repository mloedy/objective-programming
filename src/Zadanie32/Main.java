package Zadanie32;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");
        DateTimeFormatter formatterDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String line;
        do {
            LocalDate localDate = LocalDate.now();
            LocalTime localTime = LocalTime.now();
            String time = localTime.format(formatterTime);
            System.out.println("commands: date/time/datetime/quit");
            line = scanner.next();
            if(line.equals("date")){
                System.out.println(localDate);
            }else if(line.equals("time")){
                System.out.println(time);
            }else if(line.equals("datetime")){
                LocalDateTime localDateTime = LocalDateTime.now();
                String DateTime = localDateTime.format(formatterDateTime);
                System.out.println(DateTime);
            }else if (line.equals("quit")){
                System.out.println("Bye!");
                break;
            }else{
                System.out.println("Wrong command.");
            }

        }while(!line.equals("quit"));
    }
}
