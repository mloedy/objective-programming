package Zadanie1;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Ania", 70);
        Person person2 = new Person("Wojtek", 45);
        Person person3 = new Person("Jan", 30);
        System.out.println("metoda a:");
        person1.printYourNameAndage();
        person2.printYourNameAndage();
        person3.printYourNameAndage();
        System.out.println("metoda b");
        System.out.println(person1.toString());
        System.out.println(person2.toString());
        System.out.println(person3.toString());
        System.out.println("metoda c");
        System.out.println("imie: "+person1.getName()+" wiek: "+person1.getAge());
        System.out.println("imie: "+person2.getName()+" wiek: "+person2.getAge());
        System.out.println("imie: "+person3.getName()+" wiek: "+person3.getAge());
    }
}
