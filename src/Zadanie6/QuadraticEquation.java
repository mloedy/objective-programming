package Zadanie6;

public class QuadraticEquation {
    public double calculateDelta(double a, double b, double c) {
        double result = (b*b)-4*a*c;
        return result;
    }

    public double calculateX1(double a, double b, double c ) {
        double result = ((-b)-Math.sqrt((b*b)-4*a*c))/2*a;
        return result;
    }

    public double calculateX2(double a, double b, double c) {
        double result = ((-b)+Math.sqrt((b*b)-4*a*c))/2*a;
        return result;
    }

}
