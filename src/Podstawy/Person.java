package Podstawy;

public class Person {
    private String name;      //null
    private String surname;    //null
    private int age;        //0

    public Person(String name, String surname, int age) {    // konstruktor metody  (wywolanie konstruktora poprzez alt+insert
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public void printOutYourName(){
        System.out.println(name+" "+surname);
    }

    @Override                                               //
    public String toString() {           //alt insert tostring
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }
}
